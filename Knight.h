//Knight.h
#ifndef KNIGHT_H_EXISTS
#define KNIGHT_H_EXISTS

class Knight{
	private:
		int x_position;
		int y_position;
		int move_counter;
		int game_area[8][8];
	public:
		Knight();
		Knight(int x, int y, int move_counter);
		void initialize_board();
		int get_board_position_value(int x_pos, int y_pos);
		void set_board_position_value(int x_pos, int y_pos, int value);
		void print_board();
		int get_x_pos();
		void set_x_pos(int x);
		int get_y_pos();
		void set_y_pos(int y);
		int get_move_counter();
		void set_move_counter(int counter);
		void initialize_move_counter();
		void inc_move_counter();
		void dec_move_counter();
		void next_move(int current_x_pos, int current_y_pos, int iterations, int heuristic_moves);
		int count_next_move(int next_x_pos, int next_y_pos);
		void move_knight(int new_x_pos, int new_y_pos);
		void output_board_to_file();
};
#endif