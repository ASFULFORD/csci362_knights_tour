//main.cpp
#include <iostream>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include "Knight.h"
////////////////////////////////////////////////////////////////
//struct used for initial conditions
////////////////////////////////////////////////////////////////
using namespace std;
struct node{																
	int user_input_x;
	int user_input_y;
	struct node* next;
};
struct node *set_user_input(struct node *head);
struct node *createList(struct node *head);
void displayList(struct node *head);
struct node * optionList(struct node *head);
struct node * addNodeToBack(struct node *head);
struct node * deleteIthNode(struct node *head, int i);
struct node * editIthNode(struct node *head, int i);
int countList(struct node *head);
int main()
{
	////////////////////////////////////////////////////////////////
	//the main function will do the following:
	//1. create an output file
	//2. create the first node for user input
	//3. create an instance of the knight class
	//4. create a linked list of all the user's inputs and initial conditions
	//5. run the solutions as many times as the user input
	//6. output solutions to a file called output.txt
	////////////////////////////////////////////////////////////////
	std::ofstream outfile;
	outfile.open("output.txt");
	int iterations = 0, heuristic_moves = 0;
	struct node *head = new node;
	head->next = 0;
	struct node *texas_ranger = head;
	Knight knight;
	head = set_user_input(head);
	while(texas_ranger != 0)
	{
		knight.initialize_board(); 
		knight.initialize_move_counter();
		knight.inc_move_counter();
		knight.set_board_position_value(texas_ranger->user_input_x,texas_ranger->user_input_y,1);
		cout << "\nNumber of moves to attempt for the initial condition of (" << texas_ranger->user_input_x << ", " << texas_ranger->user_input_y << "): ";
		cin >> iterations;
		cout << "\nNumber of heuristic moves(1 - 64): ";
		while(1)
		{
			cin >> heuristic_moves;
			if(heuristic_moves > 1 && heuristic_moves <= 64)
			{
				knight.next_move(texas_ranger->user_input_x,texas_ranger->user_input_y, iterations, heuristic_moves);
				break;
			}
			else
				cout << "Please enter a valide number of heurstic moves: ";
		}

		outfile << "Initial condition: (" << texas_ranger->user_input_x << ", " << texas_ranger->user_input_y << ").\n";
		outfile << "Number of attempts: " << iterations << ".\n";
		outfile << "Number of Heurstic moves: " << heuristic_moves << ".\n";
		for(int j = 0; j < 8; j++)
		{
			for(int i = 0; i < 8; i++)
			{
				if(knight.get_board_position_value(i, j) == 0)
					outfile << setfill(' ') << setw(4) << -1;
				if(knight.get_board_position_value(i, j) <= 9 && knight.get_board_position_value(i, j) != 0)
					outfile << setfill(' ') << setw(4) << knight.get_board_position_value(i,j);
				if(knight.get_board_position_value(i, j) > 9)
					outfile << setfill(' ') << setw(4) << knight.get_board_position_value(i,j);
			}
			outfile << "\n";
		}
		outfile << "********************************\n\n";

		texas_ranger = texas_ranger->next;
	}
	return(0);
}
struct node *set_user_input(struct node *head)
{	
	head = createList(head);
	displayList(head);
	head = optionList(head);
	return(head);
}
struct node *createList(struct node *head)
{
	int x, y;
	char ans;

	cout << "Please enter an X_position (0 - 7): ";
	while(1)
	{
		cin >> x;
		if(x < 0 || x > 7)
		{
			cout << "Please enter a valid X_position (0 - 7): ";
		}
		else
		{
			head->user_input_x = x;
			break;
		}
	}
	cout << "Please enter an Y_position (0 - 7): ";
	while(1)
	{
		cin >> y;
		if(y < 0 || y > 7)
		{
			cout << "Please enter a valid Y_position (0 - 7): ";
		}
		else
		{
			head->user_input_y = y;
			break;
		}
	}
	cout << "Would you like to enter another position? (Y or N): ";
	cin >> ans;
	while(ans == 'y' || ans == 'Y')
	{
		head = addNodeToBack(head);
		cout << "Would you like to enter another position? (Y or N): ";
		cin >> ans;
	}
	return(head);
}
void displayList(struct node *head)
{
	struct node *texas_ranger;
	texas_ranger = head;

	while(1)
	{
		cout << "\nYour initial conditions: (" << texas_ranger->user_input_x << ", " << texas_ranger->user_input_y << ")";
		while(texas_ranger->next != 0)
		{
			texas_ranger = texas_ranger->next;
			cout << ", (" << texas_ranger->user_input_x << ", " << texas_ranger->user_input_y << ")";
		}
		cout << ".\n";
		break;
	}
}
struct node *optionList(struct node *head)
{
	int option, node_num;
	char ans;

	cout << "\nWould you like to ADD, DEL, or EDIT your initial conditions?(Y or N): ";
	cin >> ans;
	while(ans == 'y' || ans == 'Y')
	{
		cout << "\n(1) ADD\n(2) DEL\n(3) EDIT\nChoose with option you would like to preform(1 or 2 or 3): ";
		cin >> option;
		while(1)
		{
			while(1)
			{
				if(option == 1 || option == 2 || option == 3)
				{
					if(option == 1)
					{
						head = addNodeToBack(head);
						break;
					}
					if(option == 2)
					{
						cout << "\nWhich node would you like to delete?(1, 2, ..., n): ";
						cin >> node_num;
						if(countList(head) == 1)
						{
							cout << "\nYou cannot delete the only node.  Please enter a different option.";
							break;
						}
						else
						{
							if(node_num <= countList(head))
							{
								head = deleteIthNode(head, node_num);
								break;
							}
							else
							{
								cout << "\nPlease enter a valid node.\n";
								continue;
							}
						}
					}
					if(option == 3)
					{
						cout << "\nWhich node would you like to edit?(1, 2, ..., n): ";
						cin >> node_num;
						if(node_num <= countList(head))
						{
							head = editIthNode(head, node_num);
							break;
						}
						else
						{
							cout << "\nPlease enter a valid node.\n";
							continue;
						}
					}
				}
				else
				{
					cout << "\nPlease enter a valid option: ";
					cin >> option;
				}
			}
			cout << "\nWould you like to ADD, DEL, or EDIT your initial conditions?(Y or N): ";
			cin >> ans;
			break;
		}
	}
	return(head);
}
struct node *addNodeToBack(struct node *head)
{
	int x, y;
	struct node *texas_ranger = head;
	
	while(texas_ranger->next != 0) 
		texas_ranger = texas_ranger->next;

	texas_ranger->next = new node;
	texas_ranger = texas_ranger->next;
	texas_ranger->next = 0;

	cout << "Please enter an X_position (0 - 7): ";
	while(1)
	{
		cin >> x;
		if(x < 0 || x > 7)
		{
			cout << "Please enter a valid X_position (0 - 7): ";
		}
		else
		{
			texas_ranger->user_input_x = x;
			break;
		}
	}
	cout << "Please enter an Y_position (0 - 7): ";
	while(1)
	{
		cin >> y;
		if(y < 0 || y > 7)
		{
			cout << "Please enter a valid Y_position (0 - 7): ";
		}
		else
		{
			texas_ranger->user_input_y = y;
			break;
		}
	}
	displayList(head);
	return(head);
}
struct node *deleteIthNode(struct node *head, int i)
{
	struct node *texas_ranger = head;
	struct node *temp_texas_ranger = 0;
	int count = 0;
	
	if(i == 1)
	{
		head = texas_ranger->next;
	}	
	else
	{
		for(count = 0; count < i-2; count++)
			texas_ranger = texas_ranger->next;
	
		temp_texas_ranger = texas_ranger->next;
		texas_ranger->next = temp_texas_ranger->next;
	}
	return(head);
}
struct node *editIthNode(struct node *head,int i)
{
	struct node *texas_ranger = head;
	struct node *temp_texas_ranger = 0;
	int count = 0, x, y;

	if(i == 1)
	{
		cout << "Please enter a new X_position (0 - 7): ";
		while(1)
		{
			cin >> x;
			if(x < 0 || x > 7)
			{
				cout << "Please enter a valid X_position (0 - 7): ";
			}
			else
			{
				texas_ranger->user_input_x = x;
				break;
			}
		}
		cout << "Please enter a new Y_position (0 - 7): ";
		while(1)
		{
			cin >> y;
			if(y < 0 || y > 7)
			{
				cout << "Please enter a valid Y_position (0 - 7): ";
			}
			else
			{
				texas_ranger->user_input_y = y;
				break;
			}
		}
		displayList(head);
	}
	else
	{
	for(count = 0; count < i-1; count++)
		texas_ranger = texas_ranger->next;
	cout << "Please enter a new X_position (0 - 7): ";
		while(1)
		{
			cin >> x;
			if(x < 0 || x > 7)
			{
				cout << "Please enter a valid X_position (0 - 7): ";
			}
			else
			{
				texas_ranger->user_input_x = x;
				break;
			}
		}
		cout << "Please enter a new Y_position (0 - 7): ";
		while(1)
		{
			cin >> y;
			if(y < 0 || y > 7)
			{
				cout << "Please enter a valid Y_position (0 - 7): ";
			}
			else
			{
				texas_ranger->user_input_y = y;
				break;
			}
		}
	}
	displayList(head);
	return(head);
}
int countList(struct node *head)
{
	int count = 0;
	struct node *texas_ranger = head;
	while(texas_ranger != 0)
	{
		texas_ranger=texas_ranger->next;
		count++;
	}
	return(count);
}