//Knight.cpp
#include <stdio.h>
#include <iostream>
#include "Knight.h"
struct position{
	int current_x_position;
	int current_y_position;
	int previous_x_position;
	int previous_y_position;
	int move_counter;
	int previous_move_number;
	position* next;
};
position *position_head = 0;
position *position_iterator = 0;
position *temp = 0;
Knight::Knight()
{
}
Knight::Knight(int x, int y, int move_counter)
{
	Knight::x_position = x;
	Knight::y_position = y;
	Knight::move_counter = move_counter;
}
void Knight::initialize_board()
{
	////////////////////////////////////////////////////////////////
	//this function initialized the chess board with -1s
	////////////////////////////////////////////////////////////////
	for(int i = 0; i < 8; i++)
	{
		for(int j = 0; j < 8; j++)
		{
			Knight::game_area[i][j] = -1;
		}
	}
}
void Knight::print_board()
{
	////////////////////////////////////////////////////////////////
	//this function prints the chess board with specific formatting
	////////////////////////////////////////////////////////////////
	for(int j = 0; j < 8; j++)
	{
		for(int i = 0; i < 8; i++)
		{
			if(Knight::game_area[i][j] == 0)
				printf("%4d", -1);
			if(this->get_board_position_value(i, j) <= 9 && this->get_board_position_value(i, j) != 0)
				printf("%4d", this->get_board_position_value(i, j));
			if(this->get_board_position_value(i, j) > 9)
				printf("%4d", this->get_board_position_value(i, j));
		}
		printf("\n");
	}
	printf("\n\n");
}
int Knight::get_board_position_value(int x_pos, int y_pos)
{
	////////////////////////////////////////////////////////////////
	//this function returns the move number in a particular square
	////////////////////////////////////////////////////////////////
	return Knight::game_area[x_pos][y_pos];
}
void Knight::set_board_position_value(int x_pos, int y_pos, int value)
{
	////////////////////////////////////////////////////////////////
	//this function sets the move number to a particular square
	////////////////////////////////////////////////////////////////
	Knight::game_area[x_pos][y_pos] = value;
}
void Knight::set_x_pos(int x)
{
	////////////////////////////////////////////////////////////////
	//this function sets the x position of the knight
	////////////////////////////////////////////////////////////////
	Knight::x_position = x;
}
void Knight::set_y_pos(int y)
{
	////////////////////////////////////////////////////////////////
	//this function sets the y position of the knight
	////////////////////////////////////////////////////////////////
	Knight::y_position = y;
}
void Knight::set_move_counter(int counter)
{
	////////////////////////////////////////////////////////////////
	//this function sets the move counter of the knight
	////////////////////////////////////////////////////////////////
	Knight::move_counter = counter;
}
int Knight::get_x_pos()
{
	////////////////////////////////////////////////////////////////
	//this function returns the x position of the knight
	////////////////////////////////////////////////////////////////
	return Knight::x_position;
}
int Knight::get_y_pos()
{
	////////////////////////////////////////////////////////////////
	//this function returns the y position of the knight
	////////////////////////////////////////////////////////////////
	return Knight::y_position;
}
int Knight::get_move_counter()
{
	////////////////////////////////////////////////////////////////
	//this function returns the move count of the knight
	////////////////////////////////////////////////////////////////
	return Knight::move_counter;
}
void Knight::initialize_move_counter()
{
	////////////////////////////////////////////////////////////////
	//this function initializes the move count of the knight to 0
	////////////////////////////////////////////////////////////////
	Knight::move_counter = 0;
}
void Knight::inc_move_counter()
{
	////////////////////////////////////////////////////////////////
	//this function increments the move counter of the knight
	////////////////////////////////////////////////////////////////
	Knight::move_counter++;
}
void Knight::dec_move_counter()
{
	////////////////////////////////////////////////////////////////
	//this function decrements the move counter of the knight
	////////////////////////////////////////////////////////////////
	Knight::move_counter--;
}
void Knight::next_move(int current_x_pos, int current_y_pos, int iterations, int heuristic_moves)
{
	////////////////////////////////////////////////////////////////
	//this function is the bread and butter of the entire program
	//This function dos the following depending on the values 
	//from the user.  The user determines how many attempts the 
	//function will try and how many times the function will do 
	//heuristic moves.
	//There are two main section:
	//1. Heuristic moves
	//2. Depth-first moves with back-tracking
	////////////////////////////////////////////////////////////////
	int next_x_pos = 0, next_y_pos = 0, min_n_moves = 8, previous_move_number = 0, move_tracker = 1;
	bool keep_checking = true;
	////////////////////////////////////////////////////////////////
	//this first while loop ensures that the program runs until
	//the number of iterations defined by the user is satisfied and
	//whether or not the problem is solved
	////////////////////////////////////////////////////////////////
	while(move_tracker < iterations && Knight::move_counter <= 63)
	{
		////////////////////////////////////////////////////////////////
		//this second while loop lets the program run the heurist
		//method n times determined by the user
		////////////////////////////////////////////////////////////////
		while(Knight::move_counter < heuristic_moves)
		{
			////////////////////////////////////////////////////////////////
			//the 4 following if statements are similar for each of the 8
			//possible moves.  I will forgo commenting each additional move
			//since they do the same things.  I will merely mark the moves
			//IF statement tests:
			//first if: ensures that the x value of move 1 is a valid spot
			//second if: ensures that the y value of move 1 is a valid spot
			//third if: ensures that the spot has not been visited before
			//fourth if: calls a function that counts how many valid moves
			//			are available from move 1.  If it is the least, it
			//			will be stored to be the next move.
			//
			//rinse and repeat by testing the rest of the 7 possible moves
			////////////////////////////////////////////////////////////////
			if(current_x_pos + 1 <= 7 && current_x_pos + 1 >= 0)//move 1
			{
				if(current_y_pos - 2 <= 7 && current_y_pos - 2 >= 0)
				{
					if(Knight::game_area[current_x_pos + 1][current_y_pos - 2] == -1)
					{
						if(this->count_next_move(current_x_pos + 1, current_y_pos - 2) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos + 1,current_y_pos - 2);
							next_x_pos = current_x_pos + 1;
							next_y_pos = current_y_pos - 2;
						}
					}
				}
			}
			if(current_x_pos + 2 <= 7 && current_x_pos + 2 >= 0)//move 2
			{
				if(current_y_pos - 1 <= 7 && current_y_pos - 1 >= 0)
				{
					if(Knight::game_area[current_x_pos + 2][current_y_pos - 1] == -1)
					{
						if(this->count_next_move(current_x_pos + 2, current_y_pos - 1) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos + 2,current_y_pos - 1);
							next_x_pos = current_x_pos + 2;
							next_y_pos = current_y_pos - 1;
						}
					}
				}
			}
			if(current_x_pos + 2 <= 7 && current_x_pos + 2 >= 0)//move 3
			{
				if(current_y_pos + 1 <= 7 && current_y_pos + 1 >= 0)
				{
					if(Knight::game_area[current_x_pos + 2][current_y_pos + 1] == -1)
					{
						if(this->count_next_move(current_x_pos + 2, current_y_pos + 1) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos + 2,current_y_pos + 1);
							next_x_pos = current_x_pos + 2;
							next_y_pos = current_y_pos + 1;
						}
					}
				}
			}
			if(current_x_pos + 1 <= 7 && current_x_pos + 1 >= 0)//move 4
			{
				if(current_y_pos + 2 <= 7 && current_y_pos + 2 >= 0)
				{
					if(Knight::game_area[current_x_pos + 1][current_y_pos + 2] == -1)
					{
						if(this->count_next_move(current_x_pos + 1, current_y_pos + 2) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos + 1,current_y_pos + 2);
							next_x_pos = current_x_pos + 1;
							next_y_pos = current_y_pos + 2;
						}
					}
				}
			}
			if(current_x_pos - 1 <= 7 && current_x_pos - 1 >= 0)//move 5
			{
				if(current_y_pos + 2 <= 7 && current_y_pos + 2 >= 0)
				{
					if(Knight::game_area[current_x_pos - 1][current_y_pos + 2] == -1)
					{
						if(this->count_next_move(current_x_pos - 1, current_y_pos + 2) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos - 1,current_y_pos + 2);
							next_x_pos = current_x_pos - 1;
							next_y_pos = current_y_pos + 2;
						}
					}
				}
			}
			if(current_x_pos - 2 <= 7 && current_x_pos - 2 >= 0)//move 6
			{
				if(current_y_pos + 1 <= 7 && current_y_pos + 1 >= 0)
				{
					if(Knight::game_area[current_x_pos - 2][current_y_pos + 1] == -1)
					{
						if(this->count_next_move(current_x_pos - 2, current_y_pos + 1) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos - 2,current_y_pos + 1);
							next_x_pos = current_x_pos - 2;
							next_y_pos = current_y_pos + 1;
						}
					}
				}
			}
			if(current_x_pos - 2 <= 7 && current_x_pos - 2 >= 0)//move 7
			{
				if(current_y_pos - 1 <= 7 && current_y_pos - 1 >= 0)
				{
					if(Knight::game_area[current_x_pos - 2][current_y_pos - 1] == -1)
					{
						if(this->count_next_move(current_x_pos - 2, current_y_pos - 1) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos - 2,current_y_pos - 1);
							next_x_pos = current_x_pos - 2;
							next_y_pos = current_y_pos - 1;
						}
					}
				}
			}
			if(current_x_pos - 1 <= 7 && current_x_pos - 1 >= 0)//move 8
			{
				if(current_y_pos - 2 <= 7 && current_y_pos - 2 >= 0)
				{
					if(Knight::game_area[current_x_pos - 1][current_y_pos - 2] == -1)
					{
						if(this->count_next_move(current_x_pos - 1, current_y_pos - 2) <= min_n_moves)
						{
							min_n_moves = this->count_next_move(current_x_pos - 1,current_y_pos - 2);
							next_x_pos = current_x_pos - 1;
							next_y_pos = current_y_pos - 2;
						}
					}
				}
			}
			////////////////////////////////////////////////////////////////
			//now that the proper move has been selected, we implement that
			//move.
			////////////////////////////////////////////////////////////////
			this->inc_move_counter();
			this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
			this->move_knight(next_x_pos, next_y_pos);
			this->print_board();
			current_x_pos = next_x_pos;
			current_y_pos = next_y_pos;
			min_n_moves = 8;
			move_tracker++;
		}
		////////////////////////////////////////////////////////////////
		//this second while loop ensures that the program runs after
		//the number of iterations defined by the user is satisfied and
		//whether or not the problem is solved.  This section is the 
		//depth-first method, using a stack.
		//The part of the function will do the following:
		//1. test whether the proposed move has been tried before
		//2. test whether the proposed x postion is valid
		//3. test whether the proposed y position is valid
		//4. test whether the proposed (x,y) position is filled
		//5. if all are satisfied that move is set as the next move
		//6. a node is created that will hold the pertinent information
		//		and pushed onto the stack
		//7. If, after checking all 8 possible move, no valid move has 
		//		been found, the top of the stack will be accessed and 
		//		the knight will be moved back a step remembering which 
		//		it had tried previously.
		//8. The knight will attempt second route.
		//
		//
		//as stated in my report, given my algorithms and attempts at
		//this solution, my knight will only remember its previous 
		//attempt from that spot.  It will not remember any prior moves
		//Therefore, my knight will get stuck in a loop if there are 
		//2 or more possible routes after getting stuck and back
		//tracking.
		////////////////////////////////////////////////////////////////
		while(Knight::move_counter >= heuristic_moves && Knight::move_counter < 64)
		{
			position_iterator = position_head;
			keep_checking = true;
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 1)//move 1
				{
					if(current_x_pos + 1 <= 7 && current_x_pos + 1 >= 0)
					{
						if(current_y_pos - 2 <= 7 && current_y_pos - 2 >= 0)
						{
							if(Knight::game_area[current_x_pos + 1][current_y_pos - 2] == -1)
							{
								next_x_pos = current_x_pos + 1;
								next_y_pos = current_y_pos - 2;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;								
								}

								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos - 1;
								position_head->previous_y_position = current_y_pos + 2;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 1;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 2)//move 2
				{
					if(current_x_pos + 2 <= 7 && current_x_pos + 2 >= 0)
					{
						if(current_y_pos - 1 <= 7 && current_y_pos - 1 >= 0)
						{
							if(Knight::game_area[current_x_pos + 2][current_y_pos - 1] == -1)
							{
								next_x_pos = current_x_pos + 2;
								next_y_pos = current_y_pos - 1;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;							
								}

								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos - 2;
								position_head->previous_y_position = current_y_pos + 1;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 2;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 3)//move 3
				{
					if(current_x_pos + 2 <= 7 && current_x_pos + 2 >= 0)
					{
						if(current_y_pos + 1 <= 7 && current_y_pos + 1 >= 0)
						{
							if(Knight::game_area[current_x_pos + 2][current_y_pos + 1] == -1)
							{
								next_x_pos = current_x_pos + 2;
								next_y_pos = current_y_pos + 1;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;								
								}

								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos - 2;
								position_head->previous_y_position = current_y_pos - 1;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 3;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 4)//move 4
				{
					if(current_x_pos + 1 <= 7 && current_x_pos + 1 >= 0)
					{
						if(current_y_pos + 2 <= 7 && current_y_pos + 2 >= 0)
						{
							if(Knight::game_area[current_x_pos + 1][current_y_pos + 2] == -1)
							{
								next_x_pos = current_x_pos + 1;
								next_y_pos = current_y_pos + 2;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;								
								}
						
								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos - 1;
								position_head->previous_y_position = current_y_pos - 2;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 4;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 5)//move 5
				{
					if(current_x_pos - 1 <= 7 && current_x_pos - 1 >= 0)
					{
						if(current_y_pos + 2 <= 7 && current_y_pos + 2 >= 0)
						{
							if(Knight::game_area[current_x_pos - 1][current_y_pos + 2] == -1)
							{
								next_x_pos = current_x_pos - 1;
								next_y_pos = current_y_pos + 2;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;								
								}

								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos + 1;
								position_head->previous_y_position = current_y_pos - 2;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 5;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 6)//move 6
				{
					if(current_x_pos - 2 <= 7 && current_x_pos - 2 >= 0)
					{
						if(current_y_pos + 1 <= 7 && current_y_pos + 1 >= 0)
						{
							if(Knight::game_area[current_x_pos - 2][current_y_pos + 1] == -1)
							{
								next_x_pos = current_x_pos - 2;
								next_y_pos = current_y_pos + 1;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;							
								}

								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos + 2;
								position_head->previous_y_position = current_y_pos - 1;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 6;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 7)//move 7
				{
					if(current_x_pos - 2 <= 7 && current_x_pos - 2 >= 0)
					{
						if(current_y_pos - 1 <= 7 && current_y_pos - 1 >= 0)
						{
							if(Knight::game_area[current_x_pos - 2][current_y_pos - 1] == -1)
							{
								next_x_pos = current_x_pos - 2;
								next_y_pos = current_y_pos - 1;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;						
								}

								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos + 2;
								position_head->previous_y_position = current_y_pos + 1;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 7;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				if(position_iterator == 0 || previous_move_number != 8)//move 8
				{
					if(current_x_pos - 1 <= 7 && current_x_pos - 1 >= 0)
					{
						if(current_y_pos - 2 <= 7 && current_y_pos - 2 >= 0)
						{
							if(Knight::game_area[current_x_pos - 1][current_y_pos - 2] == -1)
							{
								next_x_pos = current_x_pos - 1;
								next_y_pos = current_y_pos - 2;
								this->inc_move_counter();
								this->set_board_position_value(next_x_pos, next_y_pos, Knight::move_counter);
								this->move_knight(next_x_pos, next_y_pos);
								this->print_board();
								current_x_pos = next_x_pos;
								current_y_pos = next_y_pos;

								if(position_iterator == 0)
								{
									position_head = new position;
									position_iterator = position_head;
									position_iterator->next = 0;
								}
								else
								{
									temp = new position;
									position_iterator = temp;
									position_iterator->next = position_head;
									position_head = temp;								
								}

								position_head->current_x_position = current_x_pos;
								position_head->current_y_position = current_y_pos;
								position_head->previous_x_position = current_x_pos + 1;
								position_head->previous_y_position = current_y_pos + 2;
								position_head->move_counter = Knight::move_counter;
								position_head->previous_move_number = 8;
								previous_move_number = 0;
								keep_checking = false;
								move_tracker++;
							}
						}
					}
				}
			}
			if(keep_checking)
			{
				previous_move_number = position_head->previous_move_number;
				this->dec_move_counter();
				this->set_board_position_value(position_head->current_x_position, position_head->current_y_position, -1);
				this->print_board();
				position_head = position_head->next;
				current_x_pos = position_head->current_x_position;
				current_y_pos = position_head->current_y_position;
			}
			if(move_tracker >= iterations)
				break;
		}
	}
}
int Knight::count_next_move(int next_x_pos, int next_y_pos)
{
	////////////////////////////////////////////////////////////////
	//this function returns the number of valid moves from any
	//(x,y) positions
	////////////////////////////////////////////////////////////////
	int move_counter = 0;

	if(next_x_pos + 1 <= 7 && next_x_pos + 1 >= 0)//move 1
	{
		if(next_y_pos - 2 <= 7 && next_y_pos - 2 >= 0)
		{
			if(Knight::game_area[next_x_pos + 1][next_y_pos - 2] == -1)
			{
				move_counter++;
			}
		}
	}
	if(next_x_pos + 2 <= 7 && next_x_pos + 2 >= 0)//move 2
		{
			if(next_y_pos - 1 <= 7 && next_y_pos - 1 >= 0)
			{
				if(Knight::game_area[next_x_pos + 2][next_y_pos - 1] == -1)
				{
					move_counter++;
				}
			}
		}
		if(next_x_pos + 2 <= 7 && next_x_pos + 2 >= 0)//move 3
		{
			if(next_y_pos + 1 <= 7 && next_y_pos + 1 >= 0)
			{
				if(Knight::game_area[next_x_pos + 2][next_y_pos + 1] == -1)
				{
					move_counter++;
				}
			}
		}
		if(next_x_pos + 1 <= 7 && next_x_pos + 1 >= 0)//move 4
		{
			if(next_y_pos + 2 <= 7 && next_y_pos + 2 >= 0)
			{
				if(Knight::game_area[next_x_pos + 1][next_y_pos + 2] == -1)
				{
					move_counter++;
				}
			}
		}
		if(next_x_pos - 1 <= 7 && next_x_pos - 1 >= 0)//move 5
		{
			if(next_y_pos + 2 <= 7 && next_y_pos + 2 >= 0)
			{
				if(Knight::game_area[next_x_pos - 1][next_y_pos + 2] == -1)
				{
					move_counter++;
				}
			}
		}
		if(next_x_pos - 2 <= 7 && next_x_pos - 2 >= 0)//move 6
		{
			if(next_y_pos + 1 <= 7 && next_y_pos + 1 >= 0)
			{
				if(Knight::game_area[next_x_pos - 2][next_y_pos + 1] == -1)
				{
					move_counter++;
				}
			}
		}
		if(next_x_pos - 2 <= 7 && next_x_pos - 2 >= 0)//move 7
		{
			if(next_y_pos - 1 <= 7 && next_y_pos - 1 >= 0)
			{
				if(Knight::game_area[next_x_pos - 2][next_y_pos - 1] == -1)
				{
					move_counter++;
				}
			}
		}
		if(next_x_pos - 1 <= 7 && next_x_pos - 1 >= 0)//move 8
		{
			if(next_y_pos - 2 <= 7 && next_y_pos - 2 >= 0)
			{
				if(Knight::game_area[next_x_pos - 1][next_y_pos - 2] == -1)
				{
					move_counter++;
				}
			}
		}
	
	return move_counter;
}
void Knight::move_knight(int new_x_pos, int new_y_pos)
{
	////////////////////////////////////////////////////////////////
	//this function sets the x and y position values of the knight.
	////////////////////////////////////////////////////////////////
	this->set_x_pos(new_x_pos);
	this->set_y_pos(new_y_pos);
}
